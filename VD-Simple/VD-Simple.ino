


// The simplest possible pitch-control patch for the Volca Drum's "split mode".



// Include the RK-002 cable's library
#include <RK002.h>



// RK002 declarations
RK002_DECLARE_INFO("VD-Simple", "sevenplagues@gmail.com", "0.1", "dd4305f7-73dd-470f-947e-5871e0828ebf")



bool RK002_onNoteOn(byte chan, byte key, byte velo) {

  // If this is on channel 0-5 ("1-6"), send a control-change for both layers' "pitch" and "velocity" values on the given channel.
  if (chan <= 5) {
    RK002_sendControlChange(chan, 28, key);
    RK002_sendControlChange(chan, 19, velo);
  } else if (chan == 6) { // Else, if this is on channel 6 ("7"), change the pitch and "body" of the resonator.
    RK002_sendControlChange(0, 119, key);
    RK002_sendControlChange(0, 118, velo);
  }

  // Regardless, send the NOTE-ON anyway.
  return true;
  
}



// Needs setup and loop funcs or else it won't work
void setup() {}
void loop() {}


