


// Unucr simpl pitx-kuntrol p4tx fr cu Volku Drumz "split mod".
//
// Cu frst cry drum-voysiz get individjwul pitx-kuntrol uv ytx leyur, bay sepreytiq leyurz antw ceyr on MIDI tx4nlz.
// Cyz leyurz ar kuntrold bay nots an tx4nlz 0 tu 5 (aka 1 tu 6).
//
// Cu rimeyniq cry drum-voysiz ar similrly seprreytid bay leyur, but cey wil nat txeynj cer pitx.
// Cis iz fr mor prkusiv voysiz spisifikly.
// Prkusiv-voys leyurz ar al kuntrold bay nots an txanl 6 (aka 7).
//
// Leyurz wic u laq rilys-taym wil by intrruptid bay not-afs. Cer leyur-valywm wil by set tw 0 bay en udixinl CC an ytx NOT-AF.
// WARNIQ: Meyk xr nots in yr extrnl sykwensr h4v u drreyxn, so cu leyurz cer kuntroliq dont imydyitly mywt cemselvz.
//
//
//
// Another simple pitch-control patch for the Volca Drum's "split mode".
//
// The first three drum-voices get individual pitch-control of each layer, by separating layers onto their own MIDI channels.
// These layers are controlled by notes on channels 0 to 5 (aka 1 to 6).
//
// The remaining three drum-voices are similarly separated by layer, but they will not change their pitch.
// This is for more percussive voices specifically.
// Percussive-voice layers are all controlled by notes on channel 6 (aka 7).
//
// Layers with a long release-time will be interrupted by NOTE-OFFs. Their layer-volume will be set to 0 by an additional CC on each NOTE-OFF.
// WARNING: Make sure notes in your external sequencer have a duration, so the layers they're controlling don't immediately mute themselves.



// Include the RK-002 cable's library
#include <RK002.h>



// RK002 declarations
RK002_DECLARE_INFO("VD-Simple2", "sevenplagues@gmail.com", "0.1", "15964836-2f39-4558-8c8b-55ec2220a9e1")



// Global variables
byte IS_INITIALIZED = 0; // Flag for whether initialization-CCs have already been sent or not.
byte SUSTAINS[7] = { // Each pitched-voice's current sustained note (128 when off)
  128, 128, 128, 128, 128, 128, 128
}; 



bool RK002_onNoteOn(byte chan, byte key, byte velo) {

  if (!IS_INITIALIZED) { // If initialization-CCs have not yet been sent...
  
    // Mute both layers of every voice.
    RK002_sendControlChange(0, 19, 0);
    RK002_sendControlChange(1, 19, 0);
    RK002_sendControlChange(2, 19, 0);
    RK002_sendControlChange(3, 19, 0);
    RK002_sendControlChange(4, 19, 0);
    RK002_sendControlChange(5, 19, 0);
    
    IS_INITIALIZED = 1; // Set the "is this initialized yet" flag to "yes".
    
  }

  byte layer; // Will be: current layer's value.
  byte voice; // Will be: current drum-voice.
  
  if (chan <= 5) { // If this is a pitched-voice note...
  
    layer = chan % 2; // Get the current layer that's being played (derive it from the given channel).
    voice = chan >> 1; // Since each drum-voice occupies two channels, get the voice that the current channel corresponds to.
    
    SUSTAINS[voice] = key; // Store the voice's sustained-note.
  
    RK002_sendControlChange(voice, 26 + layer, key); // Send a pitch-change to the current layer of the current voice.
    
  } else if (chan == 6) { // Else, if this is an unpitched-voice note...
  
    layer = key % 2; // Get the current layer that's being played (derive it from the given pitch).
    voice = ((key % 6) >> 1) + 3; // Unpitched voices occupy two notes per voice (one note per layer). Get the voice this note belongs to.
    
  } else { // Else, if this is on any other channel, send the note through.
  
    return true;
    
  }
  
  RK002_sendControlChange(voice, 17 + layer, velo); // Send a velocity-change to the current layer of the current drum-voice.
  
  RK002_sendNoteOn(voice, key, velo); // Send a note-on to the current drum-voice.
  
  return false; // A modified note-on has already been sent, so don't allow the original note through.

}

bool RK002_onNoteOff(byte chan, byte key, byte velo) {

  byte layer; // Will be: current layer's value.
  byte voice; // Will be: current drum-voice.
  
  if (chan <= 5) { // If this is a pitched-voice note...

    layer = chan % 2; // Get the current layer that's being played (derive it from the given channel).
    voice = chan >> 1; // Since each drum-voice occupies two channels, get the voice that the current channel corresponds to.
    
    if (SUSTAINS[voice] != key) { return false; } // If this NOTE-OFF isn't for the voice's sustained note, do nothing.
    
    SUSTAINS[voice] = 128; // Set the voice's current-sustain flag to "empty".
    
  } else if (chan == 6) { // Else, if this is an unpitched-voice note...
  
    layer = key % 2; // Get the current layer that's being played (derive it from the given pitch).
    voice = ((key % 6) >> 1) + 3; // Unpitched voices occupy two notes per voice (one note per layer). Get the voice this note belongs to.
    
  } else { // Else, if this is on any other channel, send the note through.
  
    return true;
  
  }
  
  // Otherwise, this is a NOTE-OFF for the voice's currently-sustained note. So...

  RK002_sendControlChange(voice, 17 + layer, 0); // Mute the velocity of the current layer of the current drum-voice.
  
  RK002_sendNoteOff(voice, key, velo); // Send a note-off to the current drum-voice (this is mostly a formality, in case of weird routing setups).
  
  return false; // A modified note-off has already been sent, so don't allow the original note through.

}



// Needs setup and loop funcs or else it won't work
void setup() {}
void loop() {}



